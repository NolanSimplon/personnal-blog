<?php

namespace App\Repository;

use App\Entity\Article;

class ArticleRepository
{
    public function findAll()
    {
        $articles = [];
        $connection = ConnectionDB::getConnection();
        $query = $connection->prepare("SELECT * FROM Article ORDER BY id_article DESC");
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            $articles[] = $this->sqlToBlog($line);
        }
        return $articles;
    }
    public function add(Article $article): void
    {
        $connection = ConnectionDB::getConnection();
        $query = $connection->prepare("INSERT INTO Article (id_article,author,content,date,title,image) VALUES (:id_article,:author,:content,NOW(),:title,:image)");
        $query->bindValue(":id_article", $article->id, \PDO::PARAM_INT);
        $query->bindValue(":author", $article->author, \PDO::PARAM_STR);
        $query->bindValue(":content", $article->content, \PDO::PARAM_STR);
        $query->bindValue(":title", $article->title, \PDO::PARAM_STR);
        $query->bindValue(":image", $article->image, \PDO::PARAM_STR);
        $query->execute();
        $article->id = $connection->lastInsertId();
    }
    public function find(int $id): ?Article
    {
        $connection = ConnectionDB::getConnection();
        $query = $connection->prepare("SELECT * FROM Article WHERE id_article=:id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();
        if ($line = $query->fetch()) {
            return $this->sqlToBlog($line);
        }
        return null;
    }
    public function update(Article $article): void
    {
        $connection = ConnectionDB::getConnection();
        $query = $connection->prepare("UPDATE Article SET author=:author, content=:content, date=:date, title=:title WHERE id_article=id");
        $query->bindValue(":author", $article->author, \PDO::PARAM_STR);
        $query->bindValue(":content", $article->content, \PDO::PARAM_STR);
        $query->bindValue(":date", $article->date, \PDO::PARAM_INT);
        $query->bindValue(":title", $article->title, \PDO::PARAM_STR);
        $query->bindValue(":image", $article->image, \PDO::PARAM_STR);
        $query->execute();
    }
    public function remove (Article $article): void
    {
        $connection = ConnectionDB::getConnection();
        $query = $connection->prepare("DELETE FROM Article WHERE id_article=:id");
        $query->bindValue(":id", $article->id, \PDO::PARAM_INT);
        $query->execute();
    }
    private function sqlToBlog(array $line): Article
    {
        $article = new Article();
        $article->id = intval($line["id_article"]);
        $article->author = $line["author"];
        $article->content = $line["content"];
        $article->date = ($line["date"]);
        $article->title = ($line["title"]);
        $article->image = ($line["image"]);
        return $article;
    }
    public function search(string $lettres) {
        $articles = [];
        $connection = ConnectionDB::getConnection();
        $query = $connection->prepare("SELECT * FROM Article WHERE title LIKE :lettres");
        $query->bindValue(":lettres", "%".$lettres."%");
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            $articles[] = $this->sqlToBlog($line);
        }
        return $articles;
    }
}