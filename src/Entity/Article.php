<?php

namespace App\Entity;

class Article {
    public $id;
    public $author;
    public $content;
    public $date;
    public $title;
    public $image;
}