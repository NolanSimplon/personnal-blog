<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\ArticleRepository;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends AbstractController {
    /**
     * @Route ("/", name="allArticles")
     */
    public function AllArticles( ArticleRepository $repo)
    {
        return $this->render("homepage.html.twig", 
        ["articles" => $repo->findAll()]);
    }
     /**
     * @Route ("/post"), name="form"
     */
    public function ArtForm(Request $request, ArticleRepository $repo)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repo->add($article);
            dump($article);
            return $this->redirectToRoute("allArticles");
        }
        return $this->render("/form.html.twig", ["form" => $form->createView()]);
    }
    /**
     * @Route ("/article/{id}", name="article")
     */
    public function Article(ArticleRepository $repo, int $id){
        return $this->render("/article.html.twig", ["article" => $repo->find($id)]);
    }
    /**
     * @Route ("/del/{id}", name="delArticle")
     */
    public function DelArticle (ArticleRepository $repo, int $id) {
        $article = $repo->find($id);
        $repo->remove($article);
        return $this->redirectToRoute("allArticles");
    }
    /**
     * @Route ("/search", name="search")
     */
    public function allSearched(Request $request, ArticleRepository $repo)
    {
        $articles = $repo->search($request->get("search"));
        return $this->render("searched.html.twig", ["articles" => $articles]);
    }
}